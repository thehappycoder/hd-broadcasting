package controllers

import javax.inject._

import play.api.mvc._
import play.api.libs.streams.ActorFlow
import javax.inject.Inject

import actors.PeerActor.{Broadcaster, Watcher}
import actors.{PeerActor, RoomActor, RoomsActor}
import akka.actor.{ActorSystem, Props}
import akka.stream.Materializer
import akka.util.ByteString

@Singleton
class VideoController @Inject()(cc: ControllerComponents)(implicit actorSystem: ActorSystem, mat: Materializer) extends AbstractController(cc) {
  private val rooms = actorSystem.actorOf(Props[RoomsActor], "rooms")

  def socket = WebSocket.accept[ByteString, ByteString] { request =>
    val roomId = request.getQueryString("roomId").get
    val roleString = request.getQueryString("role").get

    val role = roleString match {
      case "broadcaster" => Broadcaster
      case "watcher" => Watcher
      case _ => throw new Error(s"Unknown role $roleString")
    }

    ActorFlow.actorRef { out =>
      PeerActor.props(out, rooms, roomId, role)
    }
  }
}