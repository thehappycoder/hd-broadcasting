package actors

import actors.PeerActor.{Broadcaster, Role, Watcher}
import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.util.ByteString

import scala.collection.parallel.ParSet

object RoomActor {
  def props = Props(new RoomActor)

  case class Join(peer: ActorRef, role: Role)
  case class Leave(peer: ActorRef)
}

class RoomActor extends Actor {
  import RoomActor._

  var broadcaster: Option[ActorRef] = None
  var watchers: ParSet[ActorRef] = Set.empty[ActorRef].par

  def receive = {
    case Join(peer, role) =>
      role match {
        case Broadcaster => broadcaster = Some(peer)
        case Watcher => watchers = watchers + peer
      }

      println(s"$role joined the room ${self.path.name}")
    case Leave(peer) =>
      if (watchers contains peer) {
        watchers = watchers - peer
      } else if (broadcaster.contains(peer)) {
        broadcaster = None
      }

      if (watchers.isEmpty && broadcaster.isEmpty) {
        self ! PoisonPill
      }
    case chunk: ByteString =>
      watchers.foreach(watcher => {
        println(s"Sending chunk to watcher $watcher inside room ${self.path.name}")
        watcher ! chunk
      })
  }

  override def postStop() = {
    println(s"Room ${self.path.name} destroyed")
  }
}
