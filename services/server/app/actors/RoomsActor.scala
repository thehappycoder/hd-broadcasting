package actors

import akka.actor.{Actor, Props}

object RoomsActor {
  case class GetRoom(roomId: String)
}

class RoomsActor extends Actor {
  import RoomsActor._

  def receive = {
    case GetRoom(roomId) =>
      val room = context.child(roomId) match {
        case Some(existingRoom) => existingRoom
        case None => {
          println(s"Creating room $roomId")
          context.actorOf(Props[RoomActor], roomId)
        }
      }

      sender ! room
  }
}
