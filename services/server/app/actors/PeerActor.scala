package actors

import akka.actor.{Actor, ActorRef, Props}
import akka.util.ByteString

object PeerActor {
  def props(out: ActorRef, roomsActor: ActorRef, roomId: String, role: Role) = Props(new PeerActor(out, roomsActor, roomId, role))

  sealed trait Role
  case object Broadcaster extends Role
  case object Watcher extends Role
}

class PeerActor(out: ActorRef, roomsActor: ActorRef, roomId: String, role: PeerActor.Role) extends Actor {
  import RoomsActor._
  import RoomActor._

  roomsActor ! GetRoom(roomId)

  private var room: Option[ActorRef] = None

  def receive = {
    case room: ActorRef =>
      this.room = Some(room)
      room ! Join(out, role)
    case videoChunk: ByteString =>
      // Broadcast this video chunk that comes from broadcaster via websocket
      room.foreach(_ ! videoChunk)
  }

  override def postStop() = {
    room.foreach(_ ! Leave(out))
    println(s"$role disconnected from $roomId")
  }
}
