import * as React from 'react'
import {
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import {Broadcasting} from "./broadcasting/ui/index";
import {Watching} from "./watching";

export const Routes = () => (
  <Router>
    <Switch>
      <Route path="/broadcast/:roomId" component={Broadcasting}/>
      <Route path="/watch/:roomId" component={Watching}/>
    </Switch>
  </Router>
)
