import * as ReactDOM from 'react-dom'
import * as React from 'react'

import { Routes } from './Routes'

ReactDOM.render(
    <Routes />,
    document.getElementById('root')
)
