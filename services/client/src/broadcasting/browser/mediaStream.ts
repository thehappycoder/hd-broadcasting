export const getMediaStream = (): Promise<MediaStream> => {
  return navigator.mediaDevices.getUserMedia({
    video: {
      // width: 1280,
      // height: 720
      width: 480,
      height: 360
    },
    audio: false
  })
}
