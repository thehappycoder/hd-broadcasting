import * as localForage from "localforage"

export interface VideoChunk {
  key: string
  blob: Blob
}

export class VideoChunks {
  private localForageInstance: LocalForage

  public constructor(roomId: string) {
    this.localForageInstance = localForage.createInstance({
      driver      : localForage.INDEXEDDB,
      name        : `HD broadcasting`,
      version     : 1.0,
      storeName   : `room-${roomId}-chunks`
    })
  }

  public getAllChunks = async (): Promise<VideoChunk[]> => {
    const keys = await this.localForageInstance.keys()
    return await Promise.all(
      keys.map( key => {
        return this.localForageInstance.getItem<Blob>(key).then(blob => {
          return {
            key,
            blob
          }
        })
      })
    )
  }

  public save = async (key: string, value: Blob) => {
    await this.localForageInstance.setItem(key, value)
  }

  public remove = async (key: string) => {
    await this.localForageInstance.removeItem(key)
  }
}
