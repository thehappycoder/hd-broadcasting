import * as React from 'react'
import {BroadcastingStore} from "../stores";
import {observer} from "mobx-react";
import {Preview} from "./Preview";
import {Controls} from "./Controls";

export interface Props {
  match: {
    params: {
      roomId: string
    }
  }
}

@observer
export class Broadcasting extends React.PureComponent<Props, never> {
  private store: BroadcastingStore

  constructor(props: any) {
    super(props)

    this.store = new BroadcastingStore(this.props.match.params.roomId)
  }

  render() {
    return (
      <div>
        <Preview store={this.store}/>
        <Controls store={this.store} />
      </div>
    )
  }
}
