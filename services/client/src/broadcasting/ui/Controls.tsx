import * as React from 'react'

import {BroadcastingStore} from "../stores";
import {observer} from "mobx-react";

export interface Props {
  store: BroadcastingStore
}

export const Controls = observer((props: Props) => {
  return <div>
    {
      props.store.recording.isRecording ?
        <button onClick={props.store.recording.stop}>
          Stop recording
        </button> :
        <button onClick={props.store.recording.start} disabled={ !props.store.ws.connected }>
          Start recording
        </button>
    }
  </div>
})
