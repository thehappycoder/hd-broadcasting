import * as React from 'react'

import {BroadcastingStore} from "../stores";
import {observer} from "mobx-react";

export interface Props {
  store: BroadcastingStore
}

export const Preview = observer((props: Props) => {
  return <video src={props.store.preview.videoSrc} autoPlay muted />
})
