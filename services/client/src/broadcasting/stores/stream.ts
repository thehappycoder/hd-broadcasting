import {getMediaStream} from "../browser/mediaStream";

export class StreamStore {
  private mediaStream?: MediaStream

  public init = async () => {
    this.mediaStream = await getMediaStream()
    return this.mediaStream
  }
}
