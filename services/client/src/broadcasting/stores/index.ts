import {WsStore} from "../../common/stores/ws";
import {RecordingStore} from "./recording";
import {PreviewStore} from "../../common/stores/preview";
import {StreamStore} from "./stream";
import {VideoChunks} from "../browser/videoChunks";
import {autorun} from "mobx";

export class BroadcastingStore {
  public ws: WsStore
  public stream: StreamStore
  public preview: PreviewStore
  public recording: RecordingStore

  private videoChunks: VideoChunks

  private roomId: string

  public constructor(roomId: string) {
    this.roomId = roomId

    this.ws = new WsStore(this.roomId, 'broadcaster')
    this.stream = new StreamStore()
    this.preview = new PreviewStore()
    this.recording = new RecordingStore()
    this.videoChunks = new VideoChunks(roomId)

    this.ws.connect()

    // TODO process catch
    this.stream.init().then(stream => {
      this.preview.stream = stream
      this.recording.init(stream, this.onVideoChunk)
    })

    autorun(this.reactiveView)
  }

  private onVideoChunk = async (blob: Blob, timecode: number) => {
    if (blob.size <= 0) {
      return
    }

    // await this.videoChunks.save(timecode.toString(), blob)
  }

  private reactiveView = async () => {
    if (this.ws.connected) {
      const videoChunks = await this.videoChunks.getAllChunks()

      for (const { key, blob } of videoChunks) {
        // TODO handle error
        console.log(`Sending ${key}`)
        this.ws.send(blob)
        // TODO handle error
        // await this.videoChunks.remove(key)
      }
    }
  }
}
