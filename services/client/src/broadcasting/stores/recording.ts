declare var MediaRecorder: any

import {observable} from "mobx";

export class RecordingStore {
  @observable isRecording: boolean = false

  private recorder: any

  public init(stream: MediaStream, onVideoChunk: (blob: Blob, timecode: number) => void): void {
    this.recorder = new MediaRecorder(stream, {
      mimeType: 'video/webm; codecs=vp9'
    })

    this.recorder.ondataavailable = (event: any) => {
      onVideoChunk(event.data, event.timecode)
    }
  }

  public start = async () => {
    this.recorder.start(1000)
    this.isRecording = true
  }

  public stop = () => {
    this.recorder.stop()
    this.isRecording = false
  }
}
