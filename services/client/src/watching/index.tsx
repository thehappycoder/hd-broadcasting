import {WatchingStore} from "./stores";

import * as React from 'react'

export interface Props {
  match: {
    params: {
      roomId: string
    }
  }
}

export class Watching extends React.Component<Props, never> {
  private store: WatchingStore

  constructor(props: Props) {
    super(props)

    this.store = new WatchingStore(this.props.match.params.roomId)
  }

  render() {
    return (
      <div>
        <div>
          <video src={this.store.preview.videoSrc} autoPlay muted />
        </div>
      </div>
    )
  }
}
