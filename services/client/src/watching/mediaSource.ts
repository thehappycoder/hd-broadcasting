let mediaSource: MediaSource
let sourceBuffer: SourceBuffer

export const create = (): MediaSource => {
  mediaSource = window['mediaSource'] = new MediaSource()

  mediaSource.addEventListener('sourceclose', event => {
    console.log('onsourceclose', event)
  })

  mediaSource.addEventListener('sourceended', event => {
    console.log('onsourceended', event)
  })

  mediaSource.addEventListener('sourceopen', event => {
    console.log('sourceopen')
  })

  return mediaSource
}

export const appendBlob = async (blob: Blob) => {
  if (!sourceBuffer) {
    addSourceBuffer()
  }

  sourceBuffer.appendBuffer(await blobToArrayBuffer(blob))
}

const addSourceBuffer = () => {
  sourceBuffer = window['sourceBuffer'] = mediaSource.addSourceBuffer('video/webm; codecs="vp9"')
  sourceBuffer.mode = 'sequence'

  sourceBuffer.addEventListener('updateend', event => {
    console.log('updateend', event)
  })

  sourceBuffer.addEventListener('abort', event => {
    console.log('onabort', event)
  })

  sourceBuffer.addEventListener('error', event => {
    console.log('onerror', event)
  })
}

const blobToArrayBuffer = (blob: Blob): Promise<ArrayBuffer> => {
  const fileReader = new FileReader()

  return new Promise<ArrayBuffer>((resolve, reject) => {
    fileReader.onload = () => resolve(fileReader.result)
    fileReader.onerror = reject

    fileReader.readAsArrayBuffer(blob)
  })
}
