import {WsStore} from "../../common/stores/ws";
import * as MediaSource from "../mediaSource";
import {PreviewStore} from "../../common/stores/preview";

export class WatchingStore {
  private roomId: string

  public ws: WsStore
  public preview: PreviewStore

  public constructor(roomId: string) {
    this.roomId = roomId

    this.preview = new PreviewStore()
    this.preview.stream = MediaSource.create()

    this.ws = new WsStore(roomId, 'watcher')

    this.ws.onmessage = async (event: any) => {
      console.log('appending')
      await MediaSource.appendBlob(event.data)
    }

    this.ws.connect()
  }
}
