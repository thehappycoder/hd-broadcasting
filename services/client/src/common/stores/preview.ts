import {observable} from "mobx";

export class PreviewStore {
  @observable public videoSrc: string

  public set stream(stream: MediaStream | MediaSource) {
    this.videoSrc = URL.createObjectURL(stream)
  }
}
