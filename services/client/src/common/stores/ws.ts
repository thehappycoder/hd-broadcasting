import * as Server from '../api/server'
import {computed, observable} from "mobx";

type Mode = 'new' | 'connecting' | 'connected' | 'disconnected'

export class WsStore {
  @observable private mode: Mode = 'new'

  private roomId: string
  private role: string

  private ws?: WebSocket

  public constructor(roomId: string, role: string) {
    this.roomId = roomId
    this.role = role
  }

  @computed public get connected(): boolean {
    return this.mode === 'connected'
  }

  public connect = async () => {
    if (this.mode === 'connecting') {
      return
    }

    try {
      this.mode = 'connecting'
      this.ws = await Server.connect(this.roomId, this.role)

      this.ws.onmessage = this.onmessage

      this.ws.onclose = event => {
        console.log('Disconnected from WS: ', event)
        this.mode = 'disconnected'

        setTimeout(this.connect, 1000)
      }

      this.mode = 'connected'
    } catch (error) {
      this.mode = 'disconnected'
      setTimeout(this.connect, 1000)
      throw error
    }
  }

  public send = (blob: Blob) => {
    this.ws!.send(blob)
  }

  public onmessage: (event: MessageEvent) => void
}
