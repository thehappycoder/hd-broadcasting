export const connect = async (roomId: string, role: string): Promise<WebSocket> => {
  const ws = window['ws'] = new WebSocket(`wss://localhost:9000/ws?roomId=${roomId}&role=${role}`)

  return new Promise<WebSocket>((resolve, reject) => {
    ws.onopen = () => {
      console.log('Opened WS connection!')

      resolve(ws)
      // this.chunksStore.iterate((value: any, key: string) => {
      //   console.log(`Sending ${key} from the store`)
      //   this.ws.send(value)
      //   // this.chunksStore.removeItem(key)
      // })
    }

    ws.onerror = event => {
      console.log('ws error: ', event)
      reject()
    }

    return ws
  })
}
